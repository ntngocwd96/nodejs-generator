import createError from 'http-errors';
import express, { NextFunction, Request, Response } from 'express';
import path, { join } from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import helmet from 'helmet';
import cors from 'cors';

import indexRouter from './routes/index';

const rfs = require('rotating-file-stream');
const isProduction = process.env.NODE_ENV === 'production';
const app = express();

console.log(process.env.NODE_ENV);

app.use(helmet());
app.use(cors());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// Create logs
const accessLogStream = rfs.createStream('access.log', {
  interval: '1d',
  path: join(__dirname, '../logs'),
});

app.use(
  isProduction
    ? logger('combined', { stream: accessLogStream })
    : logger('dev'),
);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use(function (err: any, req: Request, res: Response, next: NextFunction) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  console.log('local error');
  console.log(res.locals);

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

export default app;